package epam.university;

import epam.university.model.Pizza.Pizza;
import epam.university.model.PizzaFactory;
import epam.university.model.PizzaType;
import epam.university.model.SauceType;
import epam.university.model.box.Box;
import epam.university.model.factories.DniproFactory;
import epam.university.model.sauce.Sauce;

public class Client {

  public static void main(String[] args) {
    PizzaFactory factory = new DniproFactory();

    System.out.println("Cooking in " + DniproFactory.class.getSimpleName());
    System.out.println("--------------------------------");
    Pizza pizza = factory.cookPizza(PizzaType.CHEESE);
    System.out.println(pizza);

    Box box = factory.getBox();
    box.pack();
    System.out.println("--------------------------------");

    Sauce sauce = factory.getSauce(SauceType.BROWNED);

    box.unpack();

    sauce.pour();
  }
}
