package epam.university.model.Pizza;

public abstract class Pizza {

  private String name;
  private String madeBy;

  public Pizza(String name, String madeBy) {
    this.name = name;
    this.madeBy = madeBy;
  }

  public String getMadeBy() {
    return madeBy;
  }

  public abstract void prepare();

  public abstract void bake();

  public abstract void cut();

  @Override
  public String toString() {
    return "Pizza{" +
        "name='" + name + '\'' +
        ", madeBy='" + madeBy + '\'' +
        '}';
  }
}
