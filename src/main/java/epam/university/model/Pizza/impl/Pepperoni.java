package epam.university.model.Pizza.impl;

import epam.university.model.Pizza.Pizza;

public class Pepperoni extends Pizza {

  public Pepperoni(String name, String madeBy) {
    super(name, madeBy);
  }

  public void prepare() {
    System.out.println("Preparing Pepperoni pizza");
  }

  public void bake() {
    System.out.println("Baking Pepperoni pizza");
  }

  public void cut() {
    System.out.println("Cutting Pepperoni pizza");
  }
}
