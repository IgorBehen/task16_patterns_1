package epam.university.model.Pizza.impl;

import epam.university.model.Pizza.Pizza;

public class Cheese extends Pizza {

  public Cheese(String name, String madeBy) {
    super(name, madeBy);
  }

  public void prepare() {
    System.out.println("Preparing Cheese pizza");
  }

  public void bake() {
    System.out.println("Baking Cheese pizza");
  }

  public void cut() {
    System.out.println("Cutting Cheese pizza");
  }
}
