package epam.university.model.Pizza.impl;

import epam.university.model.Pizza.Pizza;

public class Veggie extends Pizza {

  public Veggie(String name, String madeBy) {
    super(name, madeBy);
  }

  public void prepare() {
    System.out.println("Preparing veggie pizza");
  }

  public void bake() {
    System.out.println("Baking veggie pizza");
  }

  public void cut() {
    System.out.println("Cutting veggie pizza");
  }
}
