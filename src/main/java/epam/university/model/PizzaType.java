package epam.university.model;

public enum PizzaType {
  CHEESE, VEGGIE, PEPPERONI
}
