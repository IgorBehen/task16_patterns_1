package epam.university.model;

public enum SauceType {

  TOMATO, CREAMY, BROWNED
}
