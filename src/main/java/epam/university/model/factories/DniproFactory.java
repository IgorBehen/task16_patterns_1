package epam.university.model.factories;

import epam.university.model.Pizza.Pizza;
import epam.university.model.Pizza.impl.Cheese;
import epam.university.model.Pizza.impl.Pepperoni;
import epam.university.model.Pizza.impl.Veggie;
import epam.university.model.PizzaFactory;
import epam.university.model.PizzaType;
import epam.university.model.box.Box;
import epam.university.model.box.impl.DniproFactoryBox;

public class DniproFactory extends PizzaFactory {

  protected Pizza createPizza(PizzaType type) {
    pizza = null;
    if (type == PizzaType.CHEESE) {
      pizza = new Cheese("Cheese", "Dnipro");
    } else if (type == PizzaType.PEPPERONI) {
      pizza = new Pepperoni("Pepperoni", "Dnipro");
    } else if (type == PizzaType.VEGGIE) {
      pizza = new Veggie("Veggie", "Dnipro");
    }
    return pizza;
  }

  public Box getBox() {
    System.out.println("Creating box in Dnipro factory");
    return new DniproFactoryBox();
  }
}
