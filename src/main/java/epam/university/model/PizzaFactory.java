package epam.university.model;

import epam.university.model.Pizza.Pizza;
import epam.university.model.box.Box;
import epam.university.model.sauce.Sauce;

public abstract class PizzaFactory {

  protected Pizza pizza;

  protected abstract Pizza createPizza(PizzaType type);

  public Pizza cookPizza(PizzaType type) {
    pizza = createPizza(type);
    pizza.prepare();
    pizza.bake();
    pizza.cut();
    return pizza;
  }

  public abstract Box getBox();

  public Sauce getSauce(SauceType type) {
    Sauce sauce = null;
    switch (type) {
      case TOMATO:
        sauce = () -> System.out.println("Pouring TOMATO sauce on pizza");
        break;
      case CREAMY:
        sauce = () -> System.out.println("Pouring CREAMY sauce on pizza");
        break;
      case BROWNED:
        sauce = () -> System.out.println("Pouring BROWNED sauce on pizza");
        break;
    }
    return sauce;
  }
}
