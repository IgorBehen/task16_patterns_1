package epam.university.model.sauce;

@FunctionalInterface
public interface Sauce {

  void pour();
}
