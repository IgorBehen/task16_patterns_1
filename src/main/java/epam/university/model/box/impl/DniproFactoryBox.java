package epam.university.model.box.impl;

import epam.university.model.box.Box;

public class DniproFactoryBox implements Box {

  public void pack() {
    System.out.println("Packing on Dnipro factory");
  }

  public void unpack() {
    System.out.println("Unpacking on Dnipro factory");
  }
}

