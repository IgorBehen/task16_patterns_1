package epam.university.model.box.impl;

import epam.university.model.box.Box;

public class KyivFactoryBox implements Box {

  public void pack() {
    System.out.println("Packing on Kyiv factory");
  }

  public void unpack() {
    System.out.println("Unpacking on Kyiv factory");
  }
}

