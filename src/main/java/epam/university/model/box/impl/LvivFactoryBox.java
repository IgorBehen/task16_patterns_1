package epam.university.model.box.impl;

import epam.university.model.box.Box;

public class LvivFactoryBox implements Box {

  public void pack() {
    System.out.println("Packing on Lviv factory");
  }

  public void unpack() {
    System.out.println("Unpacking on Lviv factory");
  }
}
