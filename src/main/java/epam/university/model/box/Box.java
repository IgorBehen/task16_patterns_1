package epam.university.model.box;

public interface Box {

  void pack();

  void unpack();
}
